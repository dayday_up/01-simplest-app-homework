package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CalculateController {
    @GetMapping("/plus")
    public static String plus() {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                int result = i + j;
                builder.append(i).append("+").append(j).append("=").append(result);
                if (i != j) {
                    if (result > 9) {
                        builder.append(" ");
                    } else {
                        builder.append("  ");
                    }
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    @GetMapping("multiply")
    public static String multiply() {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j <= i; j++) {
                int result = i * j;
                builder.append(i).append("*").append(j).append("=").append(result);
                if (i != j) {
                    builder.append(result > 9 ? " " : "  ");
                }
            }
            builder.append("\n");
        }
        return builder.toString();
    }


}
